const listaTweets = document
    .querySelector('#lista-tweets');

eventListeners();

function eventListeners() {
    document.querySelector('#formulario')
    .addEventListener('submit', agregarTweet);
    
    listaTweets.addEventListener('click', borrarTweet);

    document.addEventListener('DOMContentLoaded', localStorageListo);
}

function agregarTweet(e) {
    e.preventDefault();

    const tweet = document.querySelector('#tweet').value;

    const enlace = document.createElement('a');
    enlace.className = 'borrar-tweet';
    enlace.innerText = 'X';

    const li = document.createElement('li');
    li.innerText = tweet;
    
    li.appendChild(enlace);
    listaTweets.appendChild(li);

    agregarTweetLocalStorage(tweet);
}

function borrarTweet(e) {
    e.preventDefault();

    if(e.target.className === 'borrar-tweet') {
        e.target.parentElement.remove();
        borrarTweetLocalStorage(e.target.parentElement.textContent);
    }
}

function agregarTweetLocalStorage(tweet) {
    let tweets = obtenerTweetsLocalStorage();
    tweets = [...tweets, tweet];
    localStorage.setItem('tweets', JSON.stringify(tweets));
}

function obtenerTweetsLocalStorage() {
    const getTweets = localStorage.getItem('tweets');
    return getTweets === null ? [] : JSON.parse(getTweets);
}

function localStorageListo() {
    const tweets = obtenerTweetsLocalStorage();
    tweets.forEach(tweet => {
        const enlace = document.createElement('a');
        enlace.className = 'borrar-tweet';
        enlace.innerText = 'X';
    
        const li = document.createElement('li');
        li.innerText = tweet;
        
        li.appendChild(enlace);
        listaTweets.appendChild(li);
    });
}

function borrarTweetLocalStorage(tweet) {
    let getTweets = obtenerTweetsLocalStorage();
    const tweetBorrar = tweet.substring(0, tweet.length - 1);
    const filterTweets = getTweets.filter(t => t !== tweetBorrar);

    localStorage.setItem('tweets', JSON.stringify(filterTweets));
}
